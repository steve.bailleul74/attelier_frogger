import static org.junit.jupiter.api.Assertions.*;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.junit.jupiter.api.Test;



class AppTest {
	/**
	 * Ceci est un test pour vérifier si la valeur de gauche change ou pas
	 */
	@Test
	void test_carChangedValueMoveLeft() {
		
		Car car  = new Car(50, 40, 60, Color.red);
			
		Car NewCar = car.moveLeft();
		assertTrue(car.x > NewCar.x);
		assertEquals(car.y, NewCar.y);
	}
	
	/**
	 * Cecç est un teste pour vérifier si la valeur de droite change ou pas
	 * 
	 */
	@Test
	void test_carChangedValueMoveRight() {
		
		Car car1  = new Car(80, 10, 30, Color.red);
			
		Car NewCar1 = car1.moveRight();
		assertTrue(car1.x < NewCar1.x);
		assertEquals(car1.y, NewCar1.y);
	}
	
	@Test
	void test_checkScoreUpHighScore()
	{

		
	}

	private String rectangle(int i, int j, int k, int l) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	/* public void test()
	 {
		GameModel model = new GameModel(5, 4, 2); 
		GameController controle = new GameController(model, redPlayer, yellowPlayer)
		GameController(GameModel model, PlayerType redPlayer, PlayerType yellowPlayer);
	 }*/
	
}
