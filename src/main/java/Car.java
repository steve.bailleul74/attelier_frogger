import java.awt.*;

public class Car extends Rectangle
{
	public static final int WIDTH = 18;
	public static final int HEIGHT = 18;

	private int speed;
	private Color color;

	/**
	 * Description de ce que fait la fonction.
	 * 
	 * @param x Qu'estce que le paramètre x
	 * @param y qu'est ce que y
	 * @param speed qu'est ce que speed
	 * @param color
	 */
	public Car(int x, int y, int speed, Color color)
	{
		super(x,y,WIDTH,HEIGHT);
		this.speed = speed;
		this.color = color;
	}
	
	/**
	 * La fonction de mouvement gauche qui retourne la valeur de l'objet de voiture "car"
	 * @return Une nouvelle voiture égale à l'ancienne déplacée vers la gauche
	 */ 
	public Car moveLeft()
	{
		return new Car(x-speed,y,speed,color);
	}
	
	
	/**
	 * La fonction de mouvement droite qui retourne la valeur de l'objet de voiture "car"
	 * @return Une nouvelle voiture égale à l'ancienne déplacée vers la droite
	 */ 
	public Car moveRight()
	{
		return new Car(x+speed,y,speed,color);
	}

	/**
	 * le fonction qui vide la valeur de couleur "color"
	 * @return vérifier si la couleur est null
	 */
	public boolean empty()
	{
		return color == null;
	}

	public void draw(Graphics g)
	{
		if(color != null)
		{
			g.setColor(color);
			g.fillRect(x,y,WIDTH,HEIGHT);
		}
	}
}